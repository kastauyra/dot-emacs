; Google C style
(c-add-style "google" google-c-style)

; Autopair
(unless emacs-24-4-or-later
  (progn
    (require 'autopair)
    (autopair-global-mode)))

; Disable linum where it makes sense and fixes performance
(require 'linum-off)

; Show matching parents
(require 'paren)
(show-paren-mode 1)

; Show whitespace errors
(require 'develock)

;; Imenu
(require 'imenu+)
(defun try-to-add-imenu ()
  (condition-case nil (imenu-add-menubar-index) (error nil)))
(add-hook 'font-lock-mode-hook 'try-to-add-imenu)

;; --------------------------
;; Programming language modes
;; --------------------------

;; Show the arguments of the currently written function in the echo area
(autoload 'turn-on-eldoc-mode "eldoc" nil t)
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)

;; ----------
;; Shell mode
;; ----------

;; In Shell mode, do not echo passwords
(add-hook 'comint-output-filter-functions
          'comint-watch-for-password-prompt
          'comint-strip-ctrl-m)

;; Colors
(require 'ansi-color)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;; ----------------------------
;; ssh mode on the top of shell
;; ----------------------------
(require 'ssh)

;; --------------------------
;; AUCTeX and other TeX stuff
;; --------------------------

;; preview-latex image type
(setq preview-image-type 'png)

;; Enable document parsing for AUCTeX
(setq TeX-auto-save t)
(setq TeX-parse-self t)

;; Make AUCTeX aware of multifile documents
(setq-default TeX-master nil)

;; AUCTeX toolbar support
(add-hook 'LaTeX-mode-hook #'LaTeX-install-toolbar)

;; Source specials
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (TeX-source-specials-mode 1)))
(setq TeX-source-specials-view-start-server t)

;; Use RefTeX
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

;; Set up -pdf option for latexmk
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (push
             '("%(-PDF)"
               (lambda ()
                 (if (and
                      (eq TeX-engine 'default)
                      (or TeX-PDF-mode TeX-DVI-via-PDFTeX))
                     "-pdf" "")))
             TeX-expand-list)))


;; Use latexmk
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (push
             '("latexmk" "latexmk %(-PDF) %s" TeX-run-TeX nil t
               :help "Run latexmk on file")
             TeX-command-list)))



;; Spellcheck on the fly
;(add-hook 'LaTeX-mode-hook 'flyspell-mode)
;(add-hook 'c-mode-hook          'flyspell-prog-mode 1)
;(add-hook 'c++-mode-hook        'flyspell-prog-mode 1)
;(add-hook 'cperl-mode-hook      'flyspell-prog-mode 1)
;(add-hook 'autoconf-mode-hook   'flyspell-prog-mode 1)
;(add-hook 'autotest-mode-hook   'flyspell-prog-mode 1)
;(add-hook 'sh-mode-hook         'flyspell-prog-mode 1)
;(add-hook 'makefile-mode-hook   'flyspell-prog-mode 1)
;(add-hook 'emacs-lisp-mode-hook 'flyspell-prog-mode 1)

;; Spellcheck on the fly multiple languages at once
;(autoload 'flyspell-babel-setup "flyspell-babel")
;(add-hook 'LaTeX-mode-hook 'flyspell-babel-setup)

;; Integrate RefTeX into AUCTeX
(setq reftex-plug-into-AUCTeX t)

;; Integrate RefTeX with bib-cite
(setq bib-cite-use-reftex-view-crossref t)

;; ----------------------------------------------
;; cmd-mode.el major mode for cmd and bat scripts
;; ----------------------------------------------
(autoload 'cmd-mode "cmd-mode" "CMD mode." t)
(setq auto-mode-alist (append '(("\\.\\(cmd\\|bat\\)$" . cmd-mode))
                              auto-mode-alist))

;; ------------------------------
;; po-mode.el for PO file editing
;; ------------------------------

(setq auto-mode-alist
   (cons '("\\.po\\'\\|\\.po\\." . po-mode) auto-mode-alist))
(autoload 'po-mode "po-mode" "Major mode for translators to edit PO files" t)

(require 'po)
(modify-coding-system-alist 'file "\\.po\\'\\|\\.po\\."
   'po-find-file-coding-system)
(autoload 'po-find-file-coding-system "po-mode")

;; ----
;; nXML
;; ----
;; Autocomplete closing tags
(setq nxml-slash-auto-complete-flag t)

;; Automatically use nXML for interesting file types
(setq auto-mode-alist
      (cons '("\\.\\(xml\\|xsl\\|rng\\|xhtml\\)\\'" . nxml-mode)
            auto-mode-alist))

;; --------
;; PHP mode
;; --------
(autoload 'php-mode "php-mode" "PHP mode" t)
(setq auto-mode-alist
      (append
       '(("\\.php$" . php-mode)
         ("\\.php5$" . php-mode))
       auto-mode-alist))

; easypg
; Still only works if there's a symlink gpg -> gpg1, and I was not able to
; find what uses gpg.
(setq epg-gpg-program "gpg1")

; --------
; org-mode
; --------
(setq org-enforce-todo-dependencies t)
(setq org-enforce-todo-checkbox-dependencies t)
; Bendra
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(define-key global-map "\C-cs" 'org-iswitchb)
(define-key global-map "\C-cc" 'org-capture)
(setq org-use-speed-commands t)
(setq org-log-done t)
(defconst main-org-file (concat org-directory "gtd.org"))
(setq org-agenda-files (list main-org-file
                             (concat org-directory "percona.org")
                             (concat org-directory "phd.org")
                             (concat org-directory "music.org")))
(setq org-default-notes-file main-org-file)
(setq org-mobile-inbox-for-pull main-org-file)
(setq org-mobile-directory "~/Dropbox/Apps/MobileOrg")
(setq org-mobile-use-encryption t)
(setq org-ctrl-k-protect-subtree t)
(setq org-support-shift-select t)
(setq org-yank-adjusted-subtrees t)

; Tags
(setq org-tag-alist '((:startgroup . nil)
                      ("@agenda" . ?a)
                      ("@anywhere" . ?w)
                      ("@call" . ?t)
                      ("@internet" . ?i)
                      ("@computer" . ?c)
                      ("@home" . ?h)
                      ("@readreview" . ?r)
                      ("@vilnius" . ?v)
                      ("@waitingfor" . ?f)
                      ("@checklist" . ?l)
                      (:endgroup . nil)
                      ("project" . ?p)
                      ("somedaymaybe" . ?s)
                      ("crypt" . ?k)))
(setq org-use-tag-inheritance '("somedaymaybe" "@readreview"))
(setq org-agenda-tags-todo-honor-ignore-options t)
(setq org-fast-tag-selection-single-key 'expert)

; Agendas
(setq org-agenda-custom-commands
      '(("c" "Calls" tags-todo "@call-somedaymaybe/!TODO")
        ("p" "Projects" tags-todo "project-somedaymaybe/!TODO")
        ("l" "Checklists" tags "@checklist-somedaymaybe")
        ("k" "Someday/maybe" tags-todo "somedaymaybe+LEVEL=2/!TODO"
         ((org-agenda-dim-blocked-tasks nil)))
        ("v" "Vilnius" tags-todo "@vilnius-somedaymaybe/!TODO")
        ("n" "Non-project tasks" tags-todo "-project-@waitingfor-somedaymaybe/!TODO"
         ((org-use-tag-inheritance '("project" "somedaymaybe"))))
        (" " "Agenda"
         ((agenda "" nil)
          (tags-todo "@anywhere-somedaymaybe|@call-somedaymaybe|@internet-somedaymaybe|@computer-somedaymaybe/!TODO"
                     ((org-agenda-overriding-header "Common next actions")
                      (org-agenda-dim-blocked-tasks 'invisible)))
          (tags-todo "@agenda-somedaymaybe/!TODO"
                     ((org-agenda-overriding-header "Agendas")
                      (org-agenda-dim-blocked-tasks 'invisible)))
          (tags-todo "@home-somedaymaybe/!TODO"
                     ((org-agenda-overriding-header "Home actions")
                      (org-agenda-dim-blocked-tasks 'invisible)))
          (tags-todo "@waitingfor-somedaymaybe/!TODO"
                     ((org-agenda-overriding-header "Waiting for")
                      (org-agenda-dim-blocked-tasks 'invisible)))
          (tags-todo "@vilnius-somedaymaybe/!TODO"
                     ((org-agenda-overriding-header "Errands")
                      (org-agenda-dim-blocked-tasks 'invisible)))
          (tags-todo "@readreview-somedaymaybe/!TODO"
                     ((org-agenda-overriding-header "Read/review")
                      (org-agenda-dim-blocked-tasks 'invisible)))
          (todo "LOGTIME"
                ((org-agenda-overriding-header "Time log actions")
                 (org-agenda-dim-blocked-tasks 'invisible)))
          (tags "-project/+DONE|+CANCELLED"
                ((org-agenda-overriding-header "Archivable tasks")
                 (org-use-tag-inheritance '("project"))))
          (todo "-@agenda-@anywhere-@call-@internet-@computer-@home-@readreview-@vilnius-@waitingfor-@checklist-project-somedaymaybe"
                ((org-agenda-overriding-header "Contextless tasks")))))))

(setq org-agenda-start-on-weekday nil)
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-todo-ignore-scheduled 'all)
(setq org-agenda-todo-ignore-deadlines 'all)
(setq org-agenda-todo-ignore-timestamp 'all)
; Let's try doing without
; (setq org-agenda-repeating-timestamp-show-all nil)
; whose replacement in 9.1 is
; (setq org-agenda-show-future-repeats nil)
; (setq org-agenda-prefer-last-repeat t)

(setq org-agenda-clock-consistency-checks
      (list
       :max-duration "6:00"
       :min-duration "0:00"
       :max-gap "0:05"
       :gap-ok-around (list "2:00" "12:30")))
(setq org-agenda-sticky t)
(setq org-agenda-window-setup 'current-window)

; Scheduling and deadlines
(setq org-deadline-warning-days 30)

; Drawers

; Clock tables
(setq org-clocktable-defaults
      (list
       :maxlevel 99
       :scope 'agenda-with-archives
       :stepskip0 t
       :fileskip0 t
       :narrow 45
       :link t
       :indent t
       :tcolumns 0))

; Logging
(setq org-log-into-drawer t)
(setq org-clock-into-drawer t)
(setq org-closed-keep-when-no-todo t)

; Refiling
(setq org-refile-targets (quote ((org-agenda-files :maxlevel . 9))))
; or 'buffer-name starting with 9.1, not much difference in my setup
(setq org-refile-use-outline-path 'file)
(setq org-refile-allow-creating-parent-nodes 'confirm)
(setq org-log-refile 'time)

(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)
(setq org-capture-templates
      '(("t" "TODO" entry (file+headline main-org-file "Tasks")
         "** TODO %?\n  %i\n  %a")
        ("i" "Inbox" entry (file+headline main-org-file "Inbox")
         "** INBOX: %?\n  %i\n  %a" :killbuffer)
        ("c" "Current" plain (clock) "" :clock-in :clock-keep)))
(setq org-todo-keywords
      '((sequence "WAITING(w)" "TODO(t)" "LOGTIME(l)"
                  "|" "DONE(d)" "CANCELLED(c)")))

(setq org-todo-keyword-faces
      '(("WAITING" . (:foreground "OrangeRed" :weight bold))
        ("LOGTIME" . (:foreground "OrangeRed" :weight bold))
        ("TODO" . (:foreground "Red" :weight bold))))

(setq org-modules
      '(org-habit org-bbdb org-bibtex org-docview org-gnus org-info
        org-irc org-mew org-mhe org-rmail org-vm org-w3m org-wl org-id)
)
(setq org-log-redeadline t)
(setq org-log-reschedule t)
(setq org-stuck-projects
      '("+project-somedaymaybe/!TODO" ("TODO") nil ""))
(setq org-todo-repeat-to-state "TODO")
(setq org-special-ctrl-a/e t)
(setq org-special-ctrl-k t)
(setq org-cycle-separator-lines 1)
; TODO: compute these columns from the defaults.el frame size calculations.
(setq org-tags-column -85)
(setq org-agenda-tags-column 'auto)
(setq org-habit-graph-column 50)

; And load everything except crypt
(require 'org-install)
(require 'org-checklist)

; org-mode encryption of selected subtrees
(require 'org-crypt)
(org-crypt-use-before-save-magic)
(setq org-crypt-key "B8D47CD8")
(setq org-crypt-disable-auto-save 'encrypt)

; org-id
(setq org-id-link-to-org-use-id t)

; Solarized-dark color theme
(setq solarized-termcolors 256)
(if emacs-24-or-later
    (progn
      (load-theme 'solarized-dark t))
  (progn
    (require 'color-theme)
    (require 'color-theme-solarized)
    (eval-after-load "color-theme"
      '(progn
         (color-theme-initialize)
         (color-theme-solarized-dark)))))

; IRC (ERC)
(require 'erc)

(setq erc-user-full-name "Laurynas Biveinis")

; Disable autopair
(add-hook 'erc-mode-hook
          (lambda ()
              (setq autopair-dont-activate t)))

(require 'erc-log)

(setq erc-log-channels-directory erc-log-dir)

; (setq erc-log-insert-log-on-open t)

(setq erc-save-buffer-on-part nil
      erc-save-queries-on-quit nil
      erc-log-write-after-send t
      erc-log-write-after-insert t)

(setq erc-join-buffer 'bury)

(defun my-erc-generate-log-file-name-channel-network
  (buffer target nick server port)
  "Generate an ERC log file name. using the channel and network name, resulting
in #channel@network.txt.
Ths function is a possible values for `erc-generate-log-file-name-function'."
  (require 'erc-networks)
  (let ((file (concat
               target "!"
               (or (with-current-buffer buffer (erc-network-name)) server)
               ".txt")))
    ;; we need a make-safe-file-name function.
    (convert-standard-filename file)))

(setq erc-generate-log-file-name-function
      'my-erc-generate-log-file-name-channel-network)

(erc-log-enable)

(setq erc-keywords '("laurynas"))

(setq erc-current-nick-highlight-type 'all)
(setq erc-keyword-highlight-type 'all)

(setq erc-track-exclude-types '("JOIN" "PART" "QUIT" "NICK" "MODE"))

(setq erc-track-faces-priority-list
      '(erc-current-nick-face erc-keyword-face))

(defadvice erc-track-find-face
  (around erc-track-find-face-promote-query activate)
  (if (erc-query-buffer-p)
      (setq ad-return-value (intern "erc-current-nick-face"))
    ad-do-it))

(setq erc-paranoid t)

; TODO: this is in-buffer highlight, right?
; (require 'erc-highlight-nicknames)
; (add-to-list 'erc-modules 'highlight-nicknames)
; (erc-update-modules)

(setq erc-autojoin-channels-alist
      '(("MPB" "#mysqldev" "#percona")
        ("freenode.net" "#percona" "#maria" "#maria-dev" "#mysql"
         "#mysql-dev")))

(require 'erc-services)
(erc-services-mode 1)

(setq erc-prompt-for-nickserv-password nil)

(setq erc-nickserv-passwords
      `((freenode (("laurynas" . ,freenode-nickserv-password)))))

(setq erc-server-coding-system '(utf-8 . utf-8))

(setq erc-server-reconnect-attempts t)

; TODO
;(require 'erc-spelling)
;(add-hook 'erc-mode-hook
          ;(lambda()
            ;(erc-spelling-mode)))

(defun start-chats ()
  "Connect to all chats."
  (interactive)
  (erc :server "irc.freenode.net" :port 6667 :nick "laurynas"))

(defun stop-chats ()
  "Disconnect from all chats."
  (interactive)
  (erc-cmd-GQ "Leaving"))

; Magit
(global-set-key (kbd "C-x g") 'magit-status)

(setq vc-handled-backends (delq 'Git vc-handled-backends))

; Wakatime
(global-wakatime-mode)

; Irony
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)

(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
(add-hook 'irony-mode-hook #'irony-eldoc)

; Flycheck
(global-flycheck-mode)
(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

; Company mode
(require 'company-irony-c-headers)
(add-hook 'after-init-hook 'global-company-mode)
(eval-after-load 'company
  '(add-to-list 'company-backends '(company-irony-c-headers company-irony)))

; SSH config
(autoload 'ssh-config-mode "ssh-config-mode" t)
(add-to-list 'auto-mode-alist '("/\\.ssh/config\\'"     . ssh-config-mode))
(add-to-list 'auto-mode-alist '("/sshd?_config\\'"      . ssh-config-mode))
(add-to-list 'auto-mode-alist '("/known_hosts\\'"       . ssh-known-hosts-mode))
(add-to-list 'auto-mode-alist '("/authorized_keys2?\\'" . ssh-authorized-keys-mode))
(add-hook 'ssh-config-mode-hook 'turn-on-font-lock)
