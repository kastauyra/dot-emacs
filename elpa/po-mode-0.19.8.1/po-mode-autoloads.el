;;; po-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "po-mode" "po-mode.el" (0 0 0 0))
;;; Generated autoloads from po-mode.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "po-mode" '("po-" "N_")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; po-mode-autoloads.el ends here
