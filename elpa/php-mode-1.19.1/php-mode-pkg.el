(define-package "php-mode" "1.19.1" "Major mode for editing PHP code"
  '((emacs "24")
    (cl-lib "0.5"))
  :keywords
  '("languages" "php")
  :url "https://github.com/ejmr/php-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
