;;; -*- no-byte-compile: t -*-
(define-package "autopair" "0.6.1" "Automagically pair braces and quotes like TextMate" '((cl-lib "0.3")) :commit "2d1eb81d12f71248ad305e70cceddf08d4fe2b39" :url "http://autopair.googlecode.com" :keywords '("convenience" "emulations"))
