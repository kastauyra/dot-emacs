; User info
(setq user-full-name "Laurynas Biveinis")
(setq user-mail-address "laurynas.biveinis@gmail.com")

; Keep all messages
(setq message-log-max t)

; Proper clipboard on Emacs 23 and earlier
(unless emacs-24-or-later
  (setq x-select-enable-clipboard t))

; Emacs 23.2+: Active region becomes primary selection
(if (symbolp 'select-active-regions)
    (setq select-active-regions t))

; Emacs 24.1+: deleting a region deletes.
(unless (or emacs-24-or-later (not (symbolp 'delete-active-region)))
    (setq delete-active-region t))

; C-k kills line including its newline
(setq kill-whole-line t)

; Emacs 23.2+: do not store duplicate kills
(if (symbolp 'kill-do-not-save-duplicates)
    (setq kill-do-not-save-duplicates t))

; Bookmarks are saved automatically
(setq bookmark-save-flag 1)

; Trailing newlines are highlighted
(if (symbolp 'indicate-empty-lines) ; Emacs 23.2+
    (setq-default indicate-empty-lines t))

; 24h time format
(setq display-time-24hr-format t)

; Should files end with newline?
(setq require-final-newline 'query)

; Display trailing whitespace
(setq-default show-trailing-whitespace t)

; Don't interrupt redraw on input
(unless emacs-24-5-or-later
  (setq redisplay-dont-pause t))

; No annoying beeps
(setq visible-bell t)

; Indentation can only insert spaces by default
(setq-default indent-tabs-mode nil)

; If already indented, complete
(if (symbolp 'tab-always-indent)
    (setq tab-always-indent 'complete))

; Diff options
(setq diff-switches "-u -p")

; Preserve hard links to the file you are editing
; From http://www.emacswiki.org/cgi-bin/wiki/DotEmacsChallenge
(setq backup-by-copying-when-linked t)

; Preserve the owner and group of the file you are editing
; From http://www.emacswiki.org/cgi-bin/wiki/DotEmacsChallenge
(setq backup-by-copying-when-mismatch t)

; Do not backup
(setq make-backup-files nil)

; Ask for initial file checking comment
(unless emacs-23-2-or-later
  (setq vc-initial-comment t))

; Use Unix-style line endings.
(setq-default buffer-file-coding-system 'utf-8-unix)

; XXI century encodings
(set-language-environment "UTF-8")

; No startup message
(setq inhibit-startup-message t)

; Initial frame positioned in the top left corner
(add-to-list 'initial-frame-alist '(top . 1))
(add-to-list 'initial-frame-alist '(left . 1))

(defun six-windows ()
  "Make frame contain 2x3 windows."
  (interactive)
  (delete-other-windows)
  (split-window-below)
  (split-window-right)
  (split-window-right)
  (windmove-down)
  (split-window-right)
  (split-window-right)
  (balance-windows))

; Treat new (empty) files as modified
(add-hook 'find-file-hooks
          '(lambda ()
             (when (not (file-exists-p (buffer-file-name)))
               (set-buffer-modified-p t))))

; Mark executable files as executable on save
(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)

; Custom keybindings
(defun smart-home ()
  "Move point to first non-whitespace character or 'beginning-of-line'.

Move point to the first non-whitespace character on this line.  If point was
already at that position, move point to the beginning of line."
  (interactive "^")
  (let ((oldpos (point)))
    (back-to-indentation)
    (and (= oldpos (point))
         (beginning-of-line)
         )
    )
  )

; Keybindings
(global-set-key "\C-cg" 'goto-line)
(global-set-key "\C-cn" 'next-error)
(global-set-key "\C-cp" 'previous-error)
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)

(global-set-key [(control shift up)] 'enlarge-window)
(global-set-key [(control shift down)] 'shrink-window)
(global-set-key [(control shift left)] 'enlarge-window-horizontally)
(global-set-key [(control shift right)] 'shrink-window-horizontally)

(global-set-key [home] 'smart-home)

; Enable some disabled commands
(put 'narrow-to-region 'disabled nil)

; No scroll bars
(if (fboundp 'set-scroll-bar-mode) (set-scroll-bar-mode nil))

; Don't bother entering search and replace args if the buffer is read-only
(defadvice query-replace-read-args (before barf-if-buffer-read-only activate)
  "Signal a `buffer-read-only' error if the current buffer is read-only."
  (barf-if-buffer-read-only))

; No toolbar
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

; Typing or <Delete> will remove selected text
(delete-selection-mode 1)

; Mouse avoidance
(if (symbolp 'make-pointer-invisible)
    (setq make-pointer-invisible t)
  (mouse-avoidance-mode))

; Enable visual feedback on selections
(setq transient-mark-mode t)

; Recent files menu
(recentf-mode)

(setq shift-select-mode t)

; Ignore file size in fontlocking
(unless emacs-24-or-later
  (setq font-lock-maximum-size nil))

; Turn on font-lock mode
(global-font-lock-mode 1)
(setq font-lock-maximum-decoration t)
(setq font-lock-support-mode 'jit-lock-mode)

; Show column number
(column-number-mode t)

; Better C-x b menu by IDO mode
(ido-mode t)

(setq ido-default-buffer-method 'selected-window)

; Kill completion buffers after completing
(defun kill-buffer-if-exists (buffer)
  "Kill the BUFFER if it exists."
  (if (buffer-live-p buffer)
      (kill-buffer buffer)))
(add-hook 'minibuffer-exit-hook
          '(lambda ()
             (progn
               (kill-buffer-if-exists "*Completions*")
               (kill-buffer-if-exists "*Ido Completions*"))))

;; Auto Fill
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; Show which function we are at
(setq which-func-modes t)
(which-function-mode)

; Soft word wrap
(if emacs-23-or-later
    (global-visual-line-mode 1))

; Nice unique buffer names
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward)
(setq uniquify-after-kill-buffer-p t)

; Automatically show images as images
(auto-image-file-mode 1)

;; -----
;; dired
;; -----
; Copy recursively
(setq dired-recursive-copies 'always)
; Delete recursively, but ask
(setq dired-recursive-deletes 'top)

(add-hook 'dired-load-hook
          (lambda ()
            (load "dired-x")))

(add-hook 'Man-mode-hook 'goto-address)

(put 'dired-find-alternate-file 'disabled nil)

(setq wdired-allow-to-change-permissions t)

(if emacs-23-2-or-later
    (setq dired-auto-revert-buffer t)
  (progn
    ; Refresh dired buffers on redisplay or buffer change
    ; From http://nflath.com/2009/07/dired/
    (defadvice switch-to-buffer-other-window
        (after auto-refresh-dired (buffer &optional norecord) activate)
      (if (equal major-mode 'dired-mode)
          (revert-buffer)))
    (defadvice switch-to-buffer
        (after auto-refresh-dired (buffer &optional norecord) activate)
      (if (equal major-mode 'dired-mode)
          (revert-buffer)))
    (defadvice display-buffer
        (after auto-refresh-dired (buffer &optional not-this-window frame)
               activate)
      (if (equal major-mode 'dired-mode)
          (revert-buffer)))
    (defadvice other-window
        (after auto-refresh-dired (arg &optional all-frame) activate)
      (if (equal major-mode 'dired-mode)
          (revert-buffer)))))

; On save, create missing parent directories automatically
; From http://atomized.org/2008/12/emacs-create-directory-before-saving/
(add-hook 'before-save-hook
          '(lambda ()
             (or (file-exists-p (file-name-directory buffer-file-name))
                 (make-directory (file-name-directory buffer-file-name) t))))

; -------
; CC Mode
; -------

;; Default indentation offset
(setq c-basic-offset 4)

;; TAB indents only if point in the beginning of the line
(setq c-tab-always-indent nil)

;; Styles I use
(setq c-default-style '((java-mode . "java")
                        (awk-mode . "awk")
                        (c++-mode . "stroustrup")
                        (other . "gnu")))

;; MySQL

(c-add-style "MySQL-5.7"
             '("K&R"
               (indent-tabs-mode . nil)
               (c-basic-offset . 2)
               (c-comment-only-line-offset . 0)
               (c-offsets-alist . ((statement-block-intro . +)
                                   (knr-argdecl-intro . 0)
                                   (substatement-open . 0)
                                   (label . -)
                                   (statement-cont . +)
                                   (arglist-intro . c-lineup-arglist-intro-after-paren)
                                   (arglist-close . c-lineup-arglist)
                                   (innamespace . 0)
                                   (inline-open . 0)
                                   (statement-case-open . +)))))

(c-add-style "InnoDB-5.7"
             '("K&R"
               (indent-tabs-mode . t)
               (c-basic-offset . 8)
               (c-comment-only-line-offset . 0)
               (c-offsets-alist . ((statement-block-intro . +)
                                   (knr-argdecl-intro . 0)
                                   (substatement-open . 0)
                                   (label . [0])
                                   (c . 0)
                                   (statement-cont . +)
                                   (arglist-intro . +)
                                   (arglist-close . c-lineup-arglist)
                                   (innamespace . 0)
                                   (inline-open . 0)
                                   (statement-case-open . +)
                                   ))
               ))

(add-to-list 'auto-mode-alist '("\\.ic\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.i\\'" . c++-mode))

(dir-locals-set-class-variables 'innodb-source-5.7
                                '((c-mode . ((c-file-style . "InnoDB-5.7")))
                                  (c++-mode . ((c-file-style . "InnoDB-5.7")))))
(dir-locals-set-class-variables 'mysql-source-5.7
                                '((c-mode . ((c-file-style . "MySQL-5.7")))
                                  (c++-mode . ((c-file-style . "MySQL-5.7")))))

(setq c-doc-comment-style
      '((c-mode . javadoc)
        (c++-mode . javadoc)))

; Grand Unified Debugger
(defun my-gud-hook ()
  (gud-tooltip-mode t))
(add-hook 'gdb-mode-hook 'my-gud-hook)
(add-hook 'sdb-mode-hook 'my-gud-hook)
(add-hook 'xdb-mode-hook 'my-gud-hook)
(add-hook 'perldb-mode-hook 'my-gud-hook)
(add-hook 'jdb-mode-hook 'my-gud-hook)

; Automatically indent pasted code
(defconst auto-indent-paste-modes '(emacs-lisp-mode c-mode c++-mode))

(defadvice yank (after indent-region activate)
  (if (member major-mode auto-indent-paste-modes)
      (indent-region (region-beginning) (region-end) nil)))

(defadvice yank-pop (after indent-region activate)
  (if (member major-mode auto-indent-paste-modes)
      (indent-region (region-beginning) (region-end) nil)))

; sql-mode
(add-to-list 'auto-mode-alist '("\\.test\\'" . sql-mode)) ; MySQL test files

; Thanks to Alexey Kopytov
(add-hook 'sql-mode-hook 'my-sql-mode-hook)
(defun my-sql-mode-hook ()
  (define-key sql-mode-map (kbd "RET") 'newline-and-indent)

  ;; Make # start a new line comment in SQL. This is a MySQL-specific
  ;; syntax.

  (modify-syntax-entry ?# "< b" sql-mode-syntax-table)
  (set-syntax-table sql-mode-syntax-table)
  )

; windmove
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings 'super))

; Workaround Emacs 25.2- security vuln
(unless emacs-25-3-or-later
  (eval-after-load "enriched"
    '(defun enriched-decode-display-prop (start end &optional param)
       (list start end))))

; Compilation
(setq compilation-scroll-output 'first-error)

(setq compilation-environment '("LANG=C"))
