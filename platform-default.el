; Defaults for system-specific setup

(defun system-specific-presetup()
  "Things that must be set on all other systems before main setup."
  ())

(defun system-specific-setup()
  "Setup specifics for all other systems."
  ())

